# emlid-postprocessing

## How to use it

There are two inputs required.
1. Output file name
2. Folder containing rover ubx files. Browse the folder using GUI

```
python postprocessing.py -o day2.csv
```

For help type
```
python postprocessing.py -h
```

