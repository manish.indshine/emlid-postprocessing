import Tkinter
import Tkconstants
import tkFileDialog
from Tkinter import *
import os
import csv
import numpy as np
import pandas as pd
import argparse
import sys

def main():

    # Getting Pos files
    pos = []
    xls = []

    # Saving to CSV file
    xls.append(['latitude', 'longitude', 'elevation',
                'std deviation', 'Status', 'File name'])

    for root, dirs, files in os.walk(location):
        for f in files:
            if os.path.splitext(f)[1].lower() == '.pos':
                pos.append(os.path.join(root, f))

    # If no file is found
    if len(pos) < 1:
        print('No Pos files found')
        sys.exit()

    # If file is found
    else:
        for i in range(len(pos)):
            cmp = 0
            file = pos[i]

            # Skip events.pos files
            if int(os.path.getsize(file)) < 10240:
                continue

            print(os.path.basename(file))
            data = list(csv.reader(open(file, 'rb'), delimiter=' '))
            data = data[10:]

            # Remove starting 200 values as it contains lot of error
            if len(data) > 200:
                data = data[200:]

            # List to Numpy conversion
            dataframe = pd.DataFrame(data)
            
            # Replacing all empty cells with nan values
            total_cols = len(dataframe.columns)
            
            for m in range(total_cols):
                dataframe[m].replace('', np.nan, inplace=True)
                
            # Removing all nan columns
            dataframe=dataframe.dropna(axis=1,how='all')
            
            # Converting dataframe to numpy array
            np_data = np.asarray(dataframe)

            # np_data = np.asarray(data)
            # print(np_data.shape)

            # Quality status
            Q = np_data[:, 5]

            fix_pt = np_data[np.where(Q == '1'), 4]
            fix_pt = np.asarray(fix_pt, dtype=float)

            if max(fix_pt.shape) > 1:
                std_fix = np.std(fix_pt)
                fix = np.median(fix_pt)
                lat_fix = np.median(np.asarray(
                    np_data[np.where(Q == '1'), 2], dtype=float))
                lon_fix = np.median(np.asarray(
                    np_data[np.where(Q == '1'), 3], dtype=float))
                fix_data = [lat_fix, lon_fix, fix, std_fix,
                            'fix', os.path.basename(file)]
                xls.append(fix_data)

            else:
                float_pt = np_data[np.where(np.asarray(
                    np_data[:, 9], dtype=float) < 0.05), 4]

                float_pt = np.asarray(float_pt, dtype=float)

                if max(float_pt.shape) > 1:
                    std_float = np.std(float_pt)
                    flot = np.median(float_pt)

                    lat_float = np.median(np.asarray(
                        np_data[np.where(np.asarray(np_data[:, 9], dtype=float) < 0.05), 2], dtype=float))

                    lon_float = np.median(np.asarray(
                        np_data[np.where(np.asarray(np_data[:, 9], dtype=float) < 0.05), 3], dtype=float))

                    float_data = [lat_float, lon_float, flot,
                                  std_float, 'float', os.path.basename(file)]
                    xls.append(float_data)
                else:
                    continue

        output = os.path.join(location, output_file)

        with open(output, 'wb') as resultFile:
            wr = csv.writer(resultFile, dialect='excel')
            for item in xls:
                wr.writerow(item)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('-o', '--output', help='Pass the output file name',
                        required=False,
                        default='output.csv')

    args = parser.parse_args()
    output_file = args.output

    # Defining root in Tkinter
    root = Tk()

    # Asking for photos location
    location = tkFileDialog.askdirectory(
        initialdir="/", title="Select processed directory location")

    main()
    sys.exit()
